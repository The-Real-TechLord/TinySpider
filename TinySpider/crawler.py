from spider import *


class Browser(Spider):

    def open(self, TARGET=None):
        self.TARGET = TARGET
        if not self.TARGET:
            raise URLNotFoundError("URL Not found ..")
        else:
            try:
                urllib.urlopen(self.TARGET)
            except:
                raise NoHostFoundError("host seems like down") 
