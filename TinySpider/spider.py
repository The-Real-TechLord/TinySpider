import urllib
import threading
import re
import urlparse
import requests

__credits__ = "Bomberman,T-Rex,Mnemosyne"


class URLNotFoundError(Exception): pass
class NoHostFoundError(Exception): pass
#class BrowserNotInitialized(Exception): pass


class Spider(object):
    __slots__ = ["urlbank", "baseurl", "links", "cache", "newlist", "TARGET"]

    def __init__(self, **kwargs):
        self.urlbank = []
        self.baseurl = None
        self.links = []
        self.cache = None
        self.newlist = None
        self.TARGET = None

    def _urls(self, data):
        threads = []
        for x in set(re.findall(re.compile('href="(.+?)"'), data)):
            _ = threading.Thread(target=self._parser,
                                 args=(urllib.unquote(x),))
            _.start()
            threads.append(_)
            if self.cache:
                if self.cache not in self.links:
                    self.links.append(self.cache)
        for t in threads:
            t.join()
        return self.links

    def _parser(self, url):
        if self.baseurl not in url:
            if not urlparse.urlparse(url).netloc:
                if url[0] == "/":
                    self.cache = self.baseurl + url
                else:
                    self.cache = self.baseurl + "/" + url
            else:
                self.cache = None
        else:
            self.cache = url

    def _getlinks(self, url):
        self.baseurl = urlparse.urlparse(url).netloc
        try:
            htmlsource = requets.get(url).text
        except:
            raise NoHostFoundError("host seems like down")
        else:
            return self._urls(htmlsource)

    def getAllinks(self):
        self.urlbank = [self.TARGET]
        self.newlist = [self.TARGET]
        while len(self.urlbank) > 0:
            try:
                links = self._getlinks(self.urlbank[0])
                self.urlbank.pop(0)
                for x in links:
                    if x not in self.newlist and self.baseurl in x:
                        self.newlist.append(x)
                        self.urlbank.append(x) 
                        yield x.replace("https://", "").replace("http://", "")
            except:
                self.urlbank.pop(0)
        # return map(lambda x:x.replace("https://", "").replace("http://", ""),
        # self.newlist)
