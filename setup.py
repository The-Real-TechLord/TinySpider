from setuptools import setup, find_packages


setup(
    name="TinySpider",
    packages=find_packages(),
    license='MIT',         
    version="0.1",
    description=("High-speed mini web-crawler library"),
    author="B3mB4m",
    author_email="b3mb4m@protonmail.com",
    url='https://github.com/b3mb4m/TinySpider',
)
